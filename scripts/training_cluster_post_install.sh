#!/bin/bash
#
# John Linford <john.linford@arm.com>
#

UNAME_BASE="student"
PASSWD_BASE="Tr@ining!"

if [ $# -lt 3 ] ; then
  echo "Usage: $0 script_name aas_license num_users [materials_url]"
  exit -1
fi
SCRIPT_NAME="$1"
AAS_LICENSE="$2"
NUSERS="$3"
MATERIALS_URL="$4"

# Install ACfL dependencies
yum -y update
yum -y install python3 python3-devel

# Create student account and group
useradd -m -U student
echo "${PASSWD_BASE}" | passwd --stdin student

# Open up permissions on new user home directories
sed -i 's/^UMASK/#UMASK/' /etc/login.defs
echo "UMASK 002" >> /etc/login.defs

# Set up homedir skel
pushd /etc/skel
if [ ! -z "$MATERIALS_URL" ] ; then
  case "$MATERIALS_URL" in
  *.tar.gz)
    curl -Ls "$MATERIALS_URL" | tar xvzf -
    materials_dir=$(basename "$MATERIALS_URL" .tar.gz)
    if [ -d "$materials_dir" ] ; then
      $( shopt -s dotglob ; mv $materials_dir/* . )
      rmdir "$materials_dir"
    fi
    ;;
  *)
    wget "$MATERIALS_URL"
  esac
fi
popd

# Create user accounts
for i in $(seq -f "%03g" 1 $NUSERS) ; do
  user_name="${UNAME_BASE}$i"
  useradd -K UID_MIN=2000 -m "$user_name" -g student
  echo "${PASSWD_BASE}$i" | passwd --stdin "$user_name"
done

# Enable password-based SSH
SSHD_CONFIG="/etc/ssh/sshd_config"
sed -i -e 's/PasswordAuthentication/#PasswordAuthentication/g' "$SSHD_CONFIG"
echo "PasswordAuthentication yes" >> "$SSHD_CONFIG"
systemctl restart sshd

# Enable student to sudo as studentXXX
echo "student ALL=(%student) NOPASSWD: ALL" > /etc/sudoers.d/99-students
chmod 600 /etc/sudoers.d/99-students

# Anyone logging in as "student" is assigned a student number and node
STUDENT_SH="/home/student/.studentsh"
cat > "$STUDENT_SH" <<"EOF"
exec {lock_fd}>/var/tmp/studentid.lock || exit 1
flock -n "$lock_fd" || { echo "ERROR: flock() failed." >&1; exit 1; }

MIN_ID=1
MAX_ID=$(grep -e student[0-9].*: /etc/passwd | wc -l)
unset STUDENT_ID
for i in $(seq -f "%03g" $MIN_ID $MAX_ID) ; do
  if [ ! -f /home/student$i/.studentid ] ; then
    trap "rm -f /home/student$i/.studentid" EXIT
    echo $i > "/home/student$i/.studentid"
    STUDENT_ID=$i
    break
  fi
done

flock -u "$lock_fd"

if [ -z "$STUDENT_ID" ] ; then
  echo
  echo "Sorry, no student accounts available.  Please try again later."
  echo
  exit 100
fi

STUDENT_USER=student$STUDENT_ID

# Check for X11 forwarding
if [ -z "$DISPLAY" ] ; then
  forward_x11=false
  slurm_x11_flags=
else
  forward_x11=true
  slurm_x11_flags="--x11"
fi

if $forward_x11 ; then
  # Import X11 cookies
  xauth list | while read line ; do
    sudo -u $STUDENT_USER -i xauth add $line
  done
fi

if ! sinfo | grep -q idle ; then
  echo
  echo "Please wait while a new compute node is provisioned."
  echo "This may take up to 10 minutes."
  echo
fi

# Workaround: AWS autoscaling only works with slurm batch jobs so
# poke the cluster to allocate new nodes as needed
( sudo -u $STUDENT_USER -i `which srun` -n 4 echo ) >/dev/null 2>&1

module load Generic-AArch64/RHEL/7/forge/20.1

echo
echo "Welcome! Your student ID is $STUDENT_ID."
echo
echo "Please Install Arm Forge Remote Client:"
echo "https://developer.arm.com/tools-and-software/server-and-hpc/arm-architecture-tools/downloads/download-arm-forge"
echo
echo "========== Arm Forge Remote Launch Settings =========="
echo "Host Name: $STUDENT_USER@cluster.arm-hpc.org"
echo "Remote Installation Directory: $(dirname $(dirname $(which ddt)))"
echo "Password: Tr@ining!$STUDENT_ID"
echo "======================================================"
echo

# Switch users and run on the compute node
sudo -u $STUDENT_USER -i `which srun` -n 4 --pty $slurm_x11_flags $SHELL

# Clear X11 cookies
if $forward_x11 ; then
  xauth list | while read line ; do
    dpyname=`echo $line | awk '{print $1}'`
    sudo -u $STUDENT_USER -i xauth remove $dpyname
  done
fi
EOF
chown root:root "$STUDENT_SH"
chmod +x "$STUDENT_SH"
echo "exec $STUDENT_SH" >> /home/student/.bash_profile

# Splatter software in opt
pushd /
curl -sL https://s3.us-east-2.amazonaws.com/org.arm-hpc.cluster/packages/opt_acfl-20.2_openmpi-4.0.4_mvapich2-2.3.4.tgz | tar xvzf -
popd

# Update modulepath
echo "/opt/arm/modulefiles" >> /usr/share/Modules/init/.modulespath

# Install AAS license
rm -f /opt/arm/licen[sc]es/* /opt/arm/forge/*/licen[sc]es/*
mkdir -p /opt/arm/licenses
ln -s /opt/arm/licenses /opt/arm/licences
curl -s -o "/opt/arm/licenses/license" -L "$AAS_LICENSE" 
for version in `ls /opt/arm/forge/` ; do
  ln -s /opt/arm/licences/license /opt/arm/forge/$version/licences/license
done
for version in `ls /opt/arm/reports/` ; do
  ln -s /opt/arm/licenses/license /opt/arm/reports/$version/licences/license
done

# Install MOTD
cat > /etc/update-motd.d/30-banner <<"EOF"
#!/bin/sh
cat <<"EOF"
                            _    _ _____   _____
     /\                    | |  | |  __ \ / ____|
    /  \   _ __ _ __ ___   | |__| | |__) | |
   / /\ \ | '__| '_ ` _ \  |  __  |  ___/| |
  / ____ \| |  | | | | | | | |  | | |    | |____
 /_/    \_\_|  |_| |_| |_| |_|  |_|_|     \_____|

            http://www.arm-hpc.org

EOF
cat >> /etc/update-motd.d/30-banner <<EOF
Download materials at $MATERIALS_URL

EOF
systemctl restart update-motd

