# aws-parallelcluster-aarch64-scripts
Scripts for standing up an aarch64 HPC cluster with AWS ParallelCluster

# Quick start

```
export AWS_DEFAULT_REGION=us-east-2
export AWS_SECRET_ACCESS_KEY=YOUR_SECRET_ACCESS_KEY
export AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY_ID

pip install -U aws-parallelcluster==2.8.0
pcluster configure
./training_cluster.sh create
```

## Create a new cluster

```
./training_cluster create
```

## Delete an existing cluster

```
./training_cluster delete
```

