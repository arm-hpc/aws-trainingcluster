#!/bin/bash
#
# John Linford <john.linford@arm.com>
#
# Creates a new HPC cluster optimized for training events, e.g. SVE hackathons.
#
HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

_COMMAND="$1"
DEFAULT_CLUSTER_NAME="$2"
DEFAULT_INITIAL_QUEUE_SIZE=4
DEFAULT_TRAINING_URL="https://gitlab.com/arm-hpc/training/arm-sve-tools/-/archive/master/arm-sve-tools-master.tar.gz"
DEFAULT_AAS_LICENSE="https://s3.us-east-2.amazonaws.com/org.arm-hpc.cluster/licenses/AWS_trial"

S3_BUCKET="org.arm-hpc.cluster"
POST_INSTALL_SCRIPT="training_cluster_post_install.sh"
POST_INSTALL_S3="s3://$S3_BUCKET/scripts/$POST_INSTALL_SCRIPT"

declare -a TEMP_FILES


function cleanup {
  for tempfile in ${TEMP_FILES[@]} ; do
    rm -f "$tempfile"
  done
}
trap cleanup exit 


function make_temp {
  dest="$1"
  tempfile=`mktemp`
  TEMP_FILES+=("$tempfile")
  eval $dest="$tempfile"
}


function input_line {
  prompt="$1"
  dest="$2"
  default="$3"
  if [ -z "$default" ] ; then
    prompt="$prompt: "
  else
    prompt="$prompt [$default]: "
  fi
  while true ; do
    read -p "$prompt" line
    line="${line:-$default}"
    if ! [ -z "$line" ] ; then
      break
    fi
  done
  eval $dest="$line"
}


function get_aws_object {
  ec2_cmd="$1"
  collection="$2"
  attribute="$3"
  predicate="$4"
  prompt="$5"
  dest="$6"
  all_obj=()
  probe_text="$(aws ec2 ${ec2_cmd} 2>&1)"
  if [ $? -ne 0 ] ; then
    echo "FATAL ERROR: Unable to execute 'aws ec2 ${ec2_cmd}'"
    echo "$probe_text"
    exit 255
  fi
  for obj in `aws ec2 ${ec2_cmd} | python -c "from __future__ import print_function; import sys, json; [print(obj[\"$attribute\"]) for obj in json.load(sys.stdin)[\"$collection\"] if $predicate]"` ; do
    all_obj+=("$obj")
  done
  for obj in ${all_obj[@]} ; do
    echo "  $obj"
  done
  input_line "$prompt" "$dest" "${all_obj[0]}"
}


function get_pcluster_config_var {
  key="$1"
  dest="$2"
  var=$(grep "$key" ~/.parallelcluster/config | awk '{print $3}')
  eval $dest="$var"
}


function get_key_pair {
  echo "Available key pairs:"
  get_aws_object "describe-key-pairs" "KeyPairs" "KeyName" "True" "Key Pair" KEY_NAME
}


function get_subnet {
  get_pcluster_config_var vpc_id VPC_ID
  get_pcluster_config_var master_subnet_id MASTER_SUBNET_ID
  get_pcluster_config_var compute_subnet_id COMPUTE_SUBNET_ID
}


function get_instance_types {
  get_pcluster_config_var master_instance_type MASTER_INSTANCE_TYPE
  get_pcluster_config_var compute_instance_type COMPUTE_INSTANCE_TYPE
}


function get_queue_size {
  echo "Checking maximum queue size..."
  if ! aws ec2 describe-account-attributes >/dev/null 2>&1 ; then
    val=20
    echo "WARNING: unable to describe account attributes. Defaulting to a limit of $val instances."
  else 
    val=`aws ec2 describe-account-attributes | python -c 'from __future__ import print_function; import sys, json; [print(vals["AttributeValue"]) for attr in json.load(sys.stdin)["AccountAttributes"] for vals in attr["AttributeValues"] if attr["AttributeName"] == "max-instances"]'`
    if [ $val -le 1 ] ; then
      echo "Error: Your AWS account is limited to $val instances, but you need at least two for a cluster."
      exit 1
    fi
  fi
  # Save one for the head node
  ((val--))
  MAX_QUEUE_SIZE=$val
  DEFAULT_INITIAL_QUEUE_SIZE=$((MAX_QUEUE_SIZE < DEFAULT_INITIAL_QUEUE_SIZE ? MAX_QUEUE_SIZE : DEFAULT_INITIAL_QUEUE_SIZE))

  while true ; do
    input_line "Initial Queue Size (1 to $MAX_QUEUE_SIZE)" INITIAL_QUEUE_SIZE "$DEFAULT_INITIAL_QUEUE_SIZE"
    if [ $INITIAL_QUEUE_SIZE -gt $MAX_QUEUE_SIZE ] ; then
      echo "Error: your AWS account supports at most $MAX_QUEUE_SIZE compute nodes."
    elif [ $INITIAL_QUEUE_SIZE -le 0 ] ; then
      echo "Error: please enter a number larger than $INITIAL_QUEUE_SIZE"
    else
      break
    fi
  done
}


function get_master_public_ip {
  instance_id=$(pcluster instances $CLUSTER_NAME | grep MasterServer | awk '{print $2}')
  MASTER_PUBLIC_IP=$(\
      aws ec2 describe-instances --instance-ids $instance_id | \
      python -c "from __future__ import print_function; \
                 import sys, json; \
                 print(json.load(sys.stdin)['Reservations'][0]['Instances'][0]['PublicIpAddress'])")
}


function create_cluster {
  # Get AWS region and credentials
  get_pcluster_config_var aws_region_name AWS_DEFAULT_REGION
  input_line "AWS Access Key ID" AWS_ACCESS_KEY_ID "$AWS_ACCESS_KEY_ID"
  input_line "AWS Secret Access Key" AWS_SECRET_ACCESS_KEY "$AWS_SECRET_ACCESS_KEY"
  export AWS_DEFAULT_REGION
  export AWS_ACCESS_KEY_ID 
  export AWS_SECRET_ACCESS_KEY

  # Get cluster config
  input_line "New Cluster Name" CLUSTER_NAME $DEFAULT_CLUSTER_NAME
  get_queue_size

  # Get AWS key pair, VPC, and subnet
  get_key_pair
  get_subnet

  # Get instance types
  get_instance_types

  # Customize cluster
  input_line "URL for Arm Allinea Studio License" AAS_LICENSE "$DEFAULT_AAS_LICENSE"
  if ! curl -sSf "$AAS_LICENSE" > /dev/null ; then
    echo "Error: Couldn't access Arm Allinea Studio license at '$AAS_LICENSE'"
    exit 1
  fi
  input_line "URL for training materials" TRAINING_URL "$DEFAULT_TRAINING_URL"

  # Write config
  make_temp CONFIG_FILE
  echo $CONFIG_FILE
  cat > "$CONFIG_FILE" <<EOF
[aws]
aws_region_name = $AWS_DEFAULT_REGION

[global]
cluster_template = default
update_check = true
sanity_check = true

[cluster default]
cluster_config_metadata = {"sections": {"cluster": ["default"], "scaling": ["default"], "vpc": ["default"]}}
key_name = $KEY_NAME
base_os = alinux2
scheduler = slurm
master_instance_type = $MASTER_INSTANCE_TYPE
compute_instance_type = $COMPUTE_INSTANCE_TYPE
initial_queue_size = $INITIAL_QUEUE_SIZE
max_queue_size = $MAX_QUEUE_SIZE
maintain_initial_size = true
vpc_settings = default
s3_read_resource = arn:aws:s3:::$S3_BUCKET/*
post_install = $POST_INSTALL_S3
post_install_args = "$AAS_LICENSE $MAX_QUEUE_SIZE $TRAINING_URL"

[vpc default]
vpc_id = $VPC_ID
master_subnet_id = $MASTER_SUBNET_ID
use_public_ips = true
EOF

  echo
  echo "Cluster config file: $CONFIG_FILE"
  cat "$CONFIG_FILE"
  echo

  # Launch
  echo "Creating cluster... this usually takes 10-20min"
  pcluster create -c "$CONFIG_FILE" "$CLUSTER_NAME"
  pcluster_success=$?

  # Get public IP address
  get_master_public_ip

  # Remind people to be tidy
  if [ $pcluster_success -eq 0 ] ; then
    echo
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "! Your cluster is up and IS NOW INCURRING CHARGES !"
    echo "!                                                 !"
    echo "! Master Node Public IP: $MASTER_PUBLIC_IP        !"
    echo "!                                                 !"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo
    echo "Don't forget to delete the cluster when you're done:"
    echo "$0 delete $CLUSTER_NAME"
    echo 
  fi
}


function delete_cluster {
  list_clusters
  input_line "Name of cluster to PERMANENTLY delete" CLUSTER_NAME $DEFAULT_CLUSTER_NAME
  echo "Are you sure you want to delete '$CLUSTER_NAME'? ALL FILES WILL BE PERMANENTLY DESTROYED!"
  input_line "Reenter the cluster name to confirm" CONFIRM_CLUSTER_NAME
  if [ "$CLUSTER_NAME" == "$CONFIRM_CLUSTER_NAME" ] ; then
    pcluster delete "$CLUSTER_NAME"
  fi
}


function list_clusters {
  echo "Active clusters:"
  pcluster list
}


function get_cluster_public_ip {
  input_line "Cluster Name" CLUSTER_NAME $DEFAULT_CLUSTER_NAME
  get_master_public_ip
  echo "$MASTER_PUBLIC_IP"
}
  
  

function print_usage {
  if [ $# -lt 1 ] ; then
    echo "Usage: $0 create|list|delete|publicip [cluster_name]"
    exit 1
  fi
}

# Check arguments
if [ $# -lt 1 ] ; then
  print_usage
  exit 1
fi

# Check environment
if ! which aws > /dev/null ; then
  echo "Please install awscli: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html"
  echo "e.g. 'pip install awscli'"
  exit 1
fi
if ! which pcluster > /dev/null ; then
  echo "Please install aws-parallelcluster: https://aws-parallelcluster.readthedocs.io/en/latest/getting_started.html"
  echo "e.g. 'pip install aws-parallelcluster'"
  exit 1
fi
for envar in AWS_SECRET_ACCESS_KEY AWS_ACCESS_KEY_ID ; do
  if [ -z $(eval echo \$$envar) ] ; then
    echo "WARNING: Environment variable '$envar' is not set"
  fi
done
if ! [ -f ~/.parallelcluster/config ] ; then
  echo "Error: please run 'pcluster config' to configure your VPC and subnets."
fi

case "$_COMMAND" in
create)
  create_cluster
  ;;
delete)
  delete_cluster
  ;;
list)
  list_clusters
  ;;
publicip)
  get_cluster_public_ip
  ;;
*)
  print_usage
  ;;
esac

